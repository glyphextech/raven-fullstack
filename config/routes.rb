Rails.application.routes.draw do

  devise_for :users

  get "documents/query"

  get "documents/index"

  get "documents/create"

  get "documents/show"

  get "documents/:id/relationship_diagram", to: "documents#relationship_diagram", as: "relationship_diagram"

  get "documents/:id/knowledge_diagram", to: "documents#knowledge_diagram", as: "knowledge_diagram"

  get "documents/:id/entity_tree_map", to: "documents#entity_tree_map", as: "entity_tree_map"

  get "documents/total_relationship_diagram"

  get "documents/total_knowledge_diagram"

  get "documents/total_entity_tree_map"

  resources :documents

  resources :users, only: [:update]

  get 'about' => 'welcome#about'

  get 'contact' => 'welcome#contact'

  get 'search' => 'documents#search'

  get 'Raven' => 'welcome#index'

  root to: 'welcome#index'

end